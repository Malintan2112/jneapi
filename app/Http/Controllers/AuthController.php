<?php
namespace App\Http\Controllers;use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
class AuthController extends Controller

{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'division_id' => 'required|string',
            'nik' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            
        ]);       
         $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'division_id'=>$request->division_id,
            'nik'=>$request->nik,
            'password' => bcrypt($request->password),
        ]);   
        
        
        $user->save();    
        $tokenResult = $user->createToken('Personal Access Token');
        $user->division_name =  $user->divisions->name; 
        return response()->json([
            'status' => true,
            'data'=>[
               'user'=> $user,
               'token'=>$tokenResult->accessToken
            ],
            'message'=>"Register Success"
        ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);       
         $credentials = request(['email', 'password']);        
         if(!Auth::attempt($credentials))
            return response()->json([
                'status' => false,
                'data'=>[],
                'message'=>"User Unauthorized"
            ], 401);        
        $user = $request->user();   
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;        
        if ($request->remember_me)
        $token->save();      
        $user->division_name =  $user->divisions->name; 

        return response()->json([
            'status' => true,
            'data'=>[
                'user'=>$user,
                'token' => $tokenResult->accessToken,
            ],
            'message'=>"Login Success"
        ]);
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();        
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}